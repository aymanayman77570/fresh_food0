import 'package:flutter/material.dart';
import 'package:fresh2_food/Models/quick_show_model.dart';
import 'package:fresh2_food/Settings/Your_order.dart';
import 'package:fresh2_food/Deliver/added.dart';

class Quick_Shop extends StatefulWidget {
  @override
  _Quick_ShopState createState() => _Quick_ShopState();
}

class _Quick_ShopState extends State<Quick_Shop> {
  int selected=0;
  List<Map<String,dynamic>> _data=[
    {
      "text":"All",
      'select': true
    }  ,
    {
      "text":"Fruits",
      'select': false
    } ,
    {
      "text":"Veg",
      'select': false
    } ,
    {
      "text":"Nuts",
      'select': false
    }
  ];
  Widget Conte(BuildContext context,String text,bool select,int index){
    return  InkWell(
      onTap: (){
        setState(() {
          _data[selected]["select"]=false;
          selected=index;
          _data[selected]["select"]=true;
        });
      },
      child: Container(
        width: 75,
        margin: EdgeInsets.symmetric(horizontal: 2),
        height: 45,
        decoration: BoxDecoration(
          color: select?Theme.of(context).accentColor:Theme.of(context).primaryColor,
          borderRadius: BorderRadius.all(Radius.circular(30)),
        ),
        child: Center(
          child: Text(text,style: TextStyle(color: select?Theme.of(context).primaryColor:Theme.of(context).highlightColor,fontSize: 22),),
        ),
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    int num_shows= shows.length;
    return Scaffold(
      backgroundColor:Theme.of(context).primaryColor,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height*0.80,
        decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(25),
                bottomRight: Radius.circular(25)
            ),
            boxShadow: [
              BoxShadow(
                color: Theme.of(context).backgroundColor,
                blurRadius: 1,
                spreadRadius: 1,
                offset: Offset(0, 2),
              )
            ]
        ),
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 40,left: 20,right: 20),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height*0.21,
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25)
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Theme.of(context).backgroundColor,
                      blurRadius: 1,
                      spreadRadius: 1,
                      offset: Offset(0, 2),
                    )
                  ]
              ),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top:15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        InkWell(
                        onTap: (){
                          setState(() {
                            _data[selected]["select"]=false;
                            selected=0;
                            _data[selected]["select"]=true;
                            for(int num=0; num <num_shows;num++)
                              {
                                shows[num].num=0;
                              }
                          });
                        }
                        ,child:Icon(Icons.delete_sweep,color: Theme.of(context).textSelectionColor,),
                    ),
                        Text("Quick Shop",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 25,fontWeight: FontWeight.bold),),
                        InkWell(onTap:(){
                          Navigator.push(context, MaterialPageRoute( builder: (BuildContext context) =>Splashadd()));
                        }
                        ,child: Icon(Icons.check))
                      ],
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height*0.038,),
                  Padding(
                    padding: const EdgeInsets.only(left:15.0),
                    child: Row(
                      children: <Widget>[
                        Conte(context, _data[0]['text'], _data[0]["select"],0),
                        Conte(context, _data[1]['text'], _data[1]["select"],1),
                        Conte(context, _data[2]['text'], _data[2]["select"],2),
                        Conte(context, _data[3]['text'], _data[3]["select"],3),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 9,),
            Container(
              height: 420,
              child: ListView(
                children: <Widget>[
                  GridView.builder(
                    shrinkWrap: true,
                    itemCount: shows.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount( crossAxisCount: 3),
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        margin: EdgeInsets.symmetric(vertical: 5,horizontal: 2),
                        padding: EdgeInsets.only(top: 9),
                        height: 150,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(height: 50,width: 50,child: Image.asset(shows[index].imageUrl)),
                            Text(shows[index].name,style: TextStyle(color: Theme.of(context).highlightColor,fontSize: 18),),
                            Container(
                              padding: EdgeInsets.all(2),
                              height: 30,
                              width: 80,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(5)),
                                  color: Theme.of(context).cardColor,
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  InkWell(onTap: (){
                                    setState(() {
                                      shows[index].num>0?shows[index].num--:0;
                                    });
                                  },
                                      child: Icon(Icons.remove,color: Theme.of(context).textSelectionColor,size: 25,)
                                  ),
                                  Text(shows[index].num.toString(),style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 22),),
                                  InkWell(
                                    onTap: (){
                                      setState(() {
                                        shows[index].num++;
                                      });
                                    }
                                  ,child: Icon(Icons.add,color: Theme.of(context).textSelectionColor,size: 25,)
                      ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
