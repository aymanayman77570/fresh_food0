import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fresh2_food/Models/home_store_model.dart';
import 'package:fresh2_food/Models/items_model.dart';
import 'package:fresh2_food/Store/items_home.dart';
import 'package:fresh2_food/Store/search_home_store.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}
List itmes_num=[
  citrus,
  tropical,
  cruci,
  allium,
  fruit,
  vegetable
];

class _HomeState extends State<Home> {
  Widget Tab_Screen(int num){
    return Column(
      children: <Widget>[
        InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=>items_Home(home: homes[num],items:itmes_num[num])));
            }
            ,child: Image.asset(homes[num].imageUrl,height: 230,width: 230,fit: BoxFit.contain,)),
        SizedBox(height: MediaQuery.of(context).size.height*0.02,),
        Text(homes[num].name,style: TextStyle(fontSize: 45,color: Theme.of(context).textSelectionColor,fontWeight: FontWeight.bold),)
      ],
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Container(
          height:MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Theme.of(context).focusColor,
          child: Column(
            children: <Widget>[
              Container(
          decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(35),
                bottomRight: Radius.circular(35)
            ),
            boxShadow: [
              BoxShadow(
                color: Theme.of(context).backgroundColor,
                blurRadius: 1,
                spreadRadius: 1,
                offset: Offset(0, 5),
              )
            ]
           ),
            height: MediaQuery.of(context).size.height*0.60,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.all(MediaQuery.of(context).size.height*0.04),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("              "),
                    Text("Store",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 30,fontWeight: FontWeight.bold),),
                    InkWell(onTap: ()=>Navigator.push(context,MaterialPageRoute(builder:(context)=>Search())),child: Icon(Icons.search,size: 35,color: Theme.of(context).textSelectionColor,))
                  ],
                ),
                SizedBox(height: MediaQuery.of(context).size.height*0.03,),
                DefaultTabController(
                    length: 2,
                    child: Container(
                      height: 300,

                      child: TabBarView(
                          children: [
                            Tab_Screen(4),
                            Tab_Screen(5),
                          ],
                      ),
                    )
                ),
                SizedBox(height: MediaQuery.of(context).size.height*0.015,),
                Text("Browse",style: TextStyle(fontSize: 18,color: Theme.of(context).highlightColor),),
              ],
            ),
          ),
              SizedBox(height: MediaQuery.of(context).size.height*0.02,),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 200,
                child: ListView.builder(
                    itemCount: homes.length-2,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context,  i){
                      return InkWell(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>items_Home(home: homes[i],items: itmes_num[i],)));
                        },
                        child: Container(
                          margin: EdgeInsets.all(MediaQuery.of(context).size.height*0.01),
                          padding: EdgeInsets.all(MediaQuery.of(context).size.height*0.01),
                          height: 100,
                          width: 150,
                          decoration: BoxDecoration(
                              color: homes[i].color,
                            borderRadius: BorderRadius.all(Radius.circular(15)),
                          ),
                          child: Column(
                            children: <Widget>[
                              Image.asset(homes[i].imageUrl,height: 125,width: 125,),
                              Text(homes[i].name,style: TextStyle(color: Theme.of(context).primaryColor,fontSize: 20,fontWeight: FontWeight.bold),),
                            ],
                          ),
                        ),
                      );
                    }
                ) ,
              )
            ],
          ),
        ),
      ),
    );
  }
}

/*


*/