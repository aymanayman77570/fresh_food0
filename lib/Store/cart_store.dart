import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fresh2_food/Deliver/deliver_home.dart';
import 'package:fresh2_food/Models/cart_store_model.dart';
class Cart extends StatefulWidget {
  @override
  _CartState createState() => _CartState();
}
class _CartState extends State<Cart> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Container(
        child:  Column(
            children: <Widget>[
        Container(
        padding: EdgeInsets.all(30),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height*0.18,
          decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(25),
                  bottomRight: Radius.circular(25)
              ),
              boxShadow: [
                BoxShadow(
                  color: Theme.of(context).backgroundColor,
                  blurRadius: 1,
                  spreadRadius: 1,
                  offset: Offset(0, 2),
                )
              ]
          ),
          child: Padding(
            padding: const EdgeInsets.only(top:15.0,left:5),
            child: Center(child: Text("Cart",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize:30,fontWeight: FontWeight.bold),)),
          ),
      ),
              Container(
                height: 340,
                child: ListView.builder(
                    itemCount: carts.length,
                    itemBuilder:(context,i){
                      return Container (
                        width: 315 ,
                        height: 60 ,
                        margin: EdgeInsets.symmetric(horizontal: 20,vertical: 6) ,
                        padding: EdgeInsets.all ( 10 ) ,
                        decoration: BoxDecoration (
                            borderRadius: BorderRadius.all ( Radius.circular ( 10 ) ) ,
                            color: Theme
                                .of ( context )
                                .focusColor
                        ) ,
                        child: Center (
                          child: Row (
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row (
                                children: <Widget>[
                                  Image.asset(carts[i].imageUrl,height: 100,width: 50,fit: BoxFit.contain,),
                                  SizedBox(width: 7,),
                                  Text ( carts[i].name , style: TextStyle ( color: Theme
                                      .of ( context )
                                      .textSelectionColor , fontSize: 18 ) , ) ,
                                ] ,
                              ) ,
                              Row(
                                children: <Widget>[
                                  Text ( carts[i].amount , style: TextStyle ( color: Theme
                                      .of ( context )
                                      .highlightColor , fontSize: 18 ) , ) ,
                                  SizedBox(width: 10,),
                                  Text ("£"+carts[i].price.toString() , style: TextStyle ( color: Theme
                                      .of ( context )
                                      .textSelectionColor , fontSize: 18 ) , ) ,
                                  SizedBox(width: 10,),
                                ],
                              ),
                            ] ,
                          ) ,
                        ) ,
                      ) ;
                }
                ),
              ),
              SizedBox(height: 10,),
              Padding(
                padding: const EdgeInsets.only(right:17.0,left:20),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("Sub-total",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 20),),
                          Text("£9.30",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 20),)
                        ],
                      ),
                      SizedBox(height: 10,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("Delivery",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 20),),
                          Text("Standard(free)",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 20),)
                        ],
                      ),
                      SizedBox(height: 10,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,

                        children: <Widget>[
                          Text("Total",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 25,fontWeight: FontWeight.bold),),
                          Text("£9.30",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 25,fontWeight: FontWeight.bold),)
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 80,),
              InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>Deliver_Home()));
                },
                child: Container(
                  width: 315,
                  height: 60,
                  decoration: BoxDecoration(
                    color: Theme.of(context).accentColor,
                    borderRadius: BorderRadius.all(Radius.circular(30)),
                  ),
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.arrow_forward,color: Colors.white,size: 20,),
                        Text(" CHECKOUT",style: TextStyle(color:Colors.white,fontSize: 20,fontWeight: FontWeight.bold),)
                      ],
                    ),
                  ),
                ),
              ),
      ]

        )
      )
    );
  }
}