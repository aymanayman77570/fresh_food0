import 'package:flutter/material.dart';
import 'package:fresh2_food/Deliver/added.dart';

class Search_Screen extends StatefulWidget {
  @override
  _Search_ScreenState createState() => _Search_ScreenState();
  final home;
  Search_Screen({this.home});
}

class _Search_ScreenState extends State<Search_Screen> {
  Widget row2(BuildContext context,String name,String name2){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(name,style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 18),),
        Text(name2,style: TextStyle(color: Theme.of(context).highlightColor,fontSize: 18),),
      ],
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: Theme.of(context).primaryColor,
        child: ListView(
          padding: EdgeInsets.all(20),
          children: <Widget>[
            SizedBox(height: 10,),
            Padding(
              padding: const EdgeInsets.only(top:20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  InkWell(onTap: (){
                    Navigator.pop(context);
                  },
                      child: Icon(Icons.arrow_back_ios,size: 28,color: Theme.of(context).highlightColor,)),
                  Text(widget.home.name,style: TextStyle(fontSize: 23,color: Theme.of(context).textSelectionColor),),
                  Text("        "),
                ],
              ),
            ),
            SizedBox(height: 30,),
            Container(
              height: 220,
              width: 320,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10))
              ),
              child: Image.asset(widget.home.imageUrl,fit: BoxFit.contain,),
            ),
            SizedBox(height: 20,),
            Container(
              width: 300,
              height: 70,
              padding: EdgeInsets.all(20),
              decoration: BoxDecoration(
                color: Theme.of(context).focusColor,
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(widget.home.name,style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 18),),
                    Text("1 Kilo",style: TextStyle(color: Theme.of(context).highlightColor,fontSize: 18),),
                    Text("8.00",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 18),),

                    Icon(Icons.keyboard_arrow_down,size: 25,color: Theme.of(context).textSelectionColor ,)
                  ],
                ),
              ),
            ),
            SizedBox(height: 20,),
            InkWell(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>Splashadd()));
              },
              child: Container(
                width: 315,
                height: 75,
                decoration: BoxDecoration(
                  color: Theme.of(context).accentColor,
                  borderRadius: BorderRadius.all(Radius.circular(40)),
                ),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.add_shopping_cart,color: Colors.white,size: 25,),
                      Text(" ADD TO CART",style: TextStyle(color:Colors.white,fontSize: 20,fontWeight: FontWeight.bold),)
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("Description",style: TextStyle(color: Theme.of(context).accentColor,fontSize: 22,fontWeight: FontWeight.w500),),
                Text(""),
              ],
            ),
            SizedBox(height: 10),
            Text(widget.home.name+" is a lovely green cruciferous vegetable. It’s healthy, delicious and nutritious, and there’s honestly nothing more you need to know."
              ,style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 19,),),
            SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("Storage",style: TextStyle(color: Theme.of(context).accentColor,fontSize: 22,fontWeight: FontWeight.w500),),
                Text(""),
              ],
            ),
            SizedBox(height: 10),
            Text("For maximum freshness, keep refrigerated.Wash before use."
              ,style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 19,),),
            SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("Origin",style: TextStyle(color: Theme.of(context).accentColor,fontSize: 22,fontWeight: FontWeight.w500),),
                Text(""),
              ],
            ),
            SizedBox(height: 10),
            Text("Produce of United Kingdom, Republic of Ireland, Germany, Italy, Netherlands, Poland, Spain, USA."
              ,style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 19,),),
            SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("Preparation & Usage",style: TextStyle(color: Theme.of(context).accentColor,fontSize: 22,fontWeight: FontWeight.w500),),
                Text(""),
              ],
            ),
            SizedBox(height: 10),
            Text("Wash before use. Trim as required."
              ,style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 19,),),
            SizedBox(height: 20,),
            Text("Nutritional Information",style: TextStyle(color: Theme.of(context).accentColor,fontSize: 22,fontWeight: FontWeight.w500),),
            SizedBox(height: 15,),
            row2(context, 'Serving Size', "250g"),
            SizedBox(height: 15,),
            row2(context, 'Calories', "455kcl"),
            SizedBox(height: 15,),
            row2(context, 'Protein', "10g"),
            SizedBox(height: 15,),
            row2(context, 'Carbohydrates', "20g"),
            SizedBox(height: 15,),
            row2(context, 'Sugar', "5g"),
            SizedBox(height: 15,),
            row2(context, 'Fibre', "5g"),
            SizedBox(height: 15,),
            row2(context, 'Fat', "2g"),
            SizedBox(height: 15,),
            row2(context, 'Saturated  Fat', "3g"),
            SizedBox(height: 15,),
            row2(context, 'Cholesterol', "20mg"),
            SizedBox(height: 15,),
            row2(context, 'Sodium', "20mg"),
            SizedBox(height: 15,),
            row2(context, 'Potassium', "20mg"),
            SizedBox(height: 15,),
            row2(context, 'Vitamin A ', "20mg"),
            SizedBox(height: 15,),
            row2(context, 'Vitamin C', "20mg"),
            SizedBox(height: 15,),
            row2(context, 'Calcium Iron', "20mg"),
            SizedBox(height: 20,),
          ],
        ),
      ),
    );
  }
}
