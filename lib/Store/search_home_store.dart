import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fresh2_food/Models/home_store_model.dart';
import 'package:fresh2_food/Store/search_screen.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: ListView(
        children: <Widget>[
          Column(
            children: <Widget>[
            Container(
              padding: EdgeInsets.all(10),
              height: MediaQuery.of(context).size.height*0.18,
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(35),
                  bottomRight: Radius.circular(35)
              ),
              boxShadow: [
                BoxShadow(
                  color: Theme.of(context).backgroundColor,
                  blurRadius: 2,
                  spreadRadius: 2,
                  offset: Offset(0, 2),
                ),
              ]
          ),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween ,
                    children: <Widget>[
                      Text(""),
                      Text("Search",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 25),),
                      InkWell(onTap: ()=>Navigator.pop(context),child: Icon(Icons.clear,size: 30,color: Theme.of(context).textSelectionColor,))
                    ],
                  ),
                  SizedBox(height: 34,),
                  Row(
                    children: <Widget>[
                      Container(
                        width: 295,
                        child: Padding(
                          padding:EdgeInsets.only(
                              left: 4,
                          ),
                          child: TextFormField(
                            keyboardType:TextInputType.text,
                            style: TextStyle(color: Theme.of(context).textSelectionColor),
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(10),
                              hintText: "What are you searching for?",
                              hintStyle: TextStyle(fontSize: 17,color:Theme.of(context).highlightColor),
                              filled: true,
                              prefixIcon: Icon(Icons.search,color: Theme.of(context).accentColor,size: 25,),
                              fillColor: Theme.of(context).primaryColor,
                              border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10)),
                                borderSide: BorderSide.none,
                              ),
                            ),
                          ),
                        ),
                      ),
                      InkWell(
                        child: Container(
                          height: 30,
                          width: 30,
                          decoration: BoxDecoration(
                            color: Theme.of(context).accentColor,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                          ),
                          child: Center(
                            child: Icon(Icons.mic,color: Theme.of(context).primaryColor,size: 28,),
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
          ),
              SizedBox(height: 30,),
            ],
      ),
          GridView.builder(
            shrinkWrap: true,
            itemCount: homes.length-1,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount( crossAxisCount: 2),
            itemBuilder: (BuildContext context, int i) {
              return InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>Search_Screen(home: homes[i],)));
                },
                child: Container(
                  margin: EdgeInsets.all(MediaQuery.of(context).size.height*0.015),
                  height: 100,
                  width: 80,
                  decoration: BoxDecoration(
                    color: Theme.of(context).cursorColor,
                    borderRadius: BorderRadius.all(Radius.circular(15)),
                  ),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5,vertical: 2),
                        child: Image.asset(homes[i].imageUrl,height: 120,width: 120,),
                      ),
                      Text(homes[i].name,style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 20),),
                    ],
                  ),
                ),
              );
            },
          ),
    ]
      )
    );
  }
}
