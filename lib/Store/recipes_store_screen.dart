import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fresh2_food/Models/recipes_store_model.dart';
import 'package:fresh2_food/Deliver/added.dart';
// ignore: camel_case_types
class Recipes_Screen extends StatefulWidget {
  final Recipes_Model recipe;
  Recipes_Screen({this.recipe});
  @override
  _Recipes_ScreenState createState() => _Recipes_ScreenState();
}
 List<bool> check=[true,true,true,false];
 int Num=3;
class _Recipes_ScreenState extends State<Recipes_Screen> {
  Widget row(BuildContext context,String name,String name2,IconData icon,int i,double pad){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          children: <Widget>[
            InkWell(
              onTap: (){
                setState(() {
                  check[i]=!check[i];
                });
              },
              child: Container(
                height: 25,
                width: 25,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                    color:check[i]? Theme.of(context).accentColor:Theme.of(context).focusColor
                ),
                child: Center(
                  child: Icon(Icons.check,color: check[i]?Theme.of(context).primaryColor:Theme.of(context).focusColor,),
                ),
              ),
            ),
            SizedBox(width: 10,),
            Text(name,style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 20),),
          ],
        ),
        Padding(
          padding:EdgeInsets.only(right: pad ),
          child: Row(
            children: <Widget>[
              Icon(icon,size: 28,color: Theme.of(context).textSelectionColor,),
              SizedBox(width: 10,),
              Text(name2,style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 20),),

            ],
          ),
        )
      ],
    );
  }
Widget row2(BuildContext context,String name,String name2){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(name,style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 18),),
        Text(name2,style: TextStyle(color: Theme.of(context).highlightColor,fontSize: 18),),
      ],
    );
}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: Theme.of(context).primaryColor,
        child: ListView(
          padding: EdgeInsets.all(20),
          children: <Widget>[
            SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(onTap: (){
                  Navigator.pop(context);
                },
                    child: Icon(Icons.arrow_back,size: 28,color: Theme.of(context).highlightColor,)),
                Text(widget.recipe.name,style: TextStyle(fontSize: 23,color: Theme.of(context).textSelectionColor),),
                Icon(Icons.file_upload,size: 28,color: Theme.of(context).highlightColor,),
              ],
            ),
            SizedBox(height: 30,),
            Container(
              height: 220,
              width: 320,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10))
              ),
              child: Image.asset(widget.recipe.imageUrl,fit: BoxFit.fill,),
            ),
            SizedBox(height: 40,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("Ingredients",style: TextStyle(color: Theme.of(context).accentColor,fontSize: 22,fontWeight: FontWeight.w500),),
                Text(""),
              ],
            ),
            SizedBox(height: 15,),
            row(context, '4', "Easy", Icons.filter_list,0,50),
            SizedBox(height: 12),
            row(context, 'Raspberries', "Prep 20m", Icons.access_time,1,5),
            SizedBox(height: 12,),
            row(context, '1 Strawberry', "Cook 5m", Icons.highlight,2,12),
            SizedBox(height: 12,),
            row(context, '1/2 Lemon', "", null,3,0),
            SizedBox(height: 30,),
            Text("Instructions",style: TextStyle(color: Theme.of(context).accentColor,fontSize: 22,fontWeight: FontWeight.w500),),
            SizedBox(height: 10),
            Text("Start by taking the 4 raspberries, chop them \ninto tiny segments and introduce the \nstrawberry. Check to make sure that the \nraspberries and the strawberry sit well \ntogether, before slicing and dicing a lemon \nand adding it to this rather strange \ncombination of fruits.\n"

                "\nPeel the 2 cherries, if it’s even possible to \npeel a cherry, discard the stalks and \nplace them neatly next to the other fruits.\n"

                "\nFinish off this imaginary recipe by \nsourcing a mint leaf, and place it perfectly in \nthe center of the bowl, taking care not \nto upset the other fruits that have already been placed.\n",
              style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 18),),
            SizedBox(height: 20,),
            Text("Nutritional Information",style: TextStyle(color: Theme.of(context).accentColor,fontSize: 22,fontWeight: FontWeight.w500),),
            SizedBox(height: 15,),
            row2(context, 'Serving Size', "250g"),
            SizedBox(height: 15,),
            row2(context, 'Calories', "455kcl"),
            SizedBox(height: 15,),
            row2(context, 'Protein', "10g"),
            SizedBox(height: 15,),
            row2(context, 'Carbohydrates', "20g"),
            SizedBox(height: 15,),
            row2(context, 'Sugar', "5g"),
            SizedBox(height: 15,),
            row2(context, 'Fibre', "5g"),
            SizedBox(height: 15,),
            row2(context, 'Fat', "2g"),
            SizedBox(height: 15,),
            row2(context, 'Saturated  Fat', "3g"),
            SizedBox(height: 15,),
            row2(context, 'Cholesterol', "20mg"),
            SizedBox(height: 15,),
            row2(context, 'Sodium', "20mg"),
            SizedBox(height: 15,),
            row2(context, 'Potassium', "20mg"),
            SizedBox(height: 15,),
            row2(context, 'Vitamin A ', "20mg"),
            SizedBox(height: 15,),
            row2(context, 'Vitamin C', "20mg"),
            SizedBox(height: 15,),
            row2(context, 'Calcium Iron', "20mg"),
            SizedBox(height: 40,),
            InkWell(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>Splashadd()));
              },
              child: Container(
                width: 315,
                height: 60,
                decoration: BoxDecoration(
                  color: Theme.of(context).accentColor,
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                ),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.add_shopping_cart,color: Colors.white,size: 25,),
                      Text(" Add 3 Ingredients to Cart",style: TextStyle(color:Colors.white,fontSize: 20,fontWeight: FontWeight.bold),)
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: 20,),
          ],
        ),
      ),
    );
  }
}
