import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fresh2_food/Models/recipes_store_model.dart';
import 'package:fresh2_food/Store/recipes_store_screen.dart';

class Recipes extends StatefulWidget {
  @override
  _RecipesState createState() => _RecipesState();
}

class _RecipesState extends State<Recipes> {
  int selected=0;
  List<Map<String,dynamic>> _data=[
    {
      "text":"All",
      'select': true
    }  ,
{
      "text":"Vegan",
      'select': false
    } ,
{
      "text":"Keto",
      'select': false
    } ,
{
      "text":"Paleo",
      'select': false
    }
  ];
  Widget Conte(BuildContext context,String text,bool select,int index){
    return  InkWell(
      onTap: (){
        setState(() {
          _data[selected]["select"]=false;
          selected=index;
          _data[selected]["select"]=true;
        });
      },
      child: Container(
        width: 70,
        margin: EdgeInsets.symmetric(horizontal: 2),
        height: 45,
        decoration: BoxDecoration(
          color: select?Theme.of(context).accentColor:Theme.of(context).primaryColor,
          borderRadius: BorderRadius.all(Radius.circular(30)),
        ),
        child: Center(
          child: Text(text,style: TextStyle(color: select?Colors.white:Theme.of(context).textSelectionColor,fontSize: 22),),
        ),
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Container(
        color:Theme.of(context).primaryColor,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Padding(
          padding: const EdgeInsets.only(top:12.0),
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(25),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height*0.18,
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(25),
                        bottomRight: Radius.circular(25)
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Theme.of(context).backgroundColor,
                        blurRadius: 1,
                        spreadRadius: 1,
                        offset: Offset(0, 2),
                      )
                    ]
                ),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding:  EdgeInsets.only(top:16.0),
                      child: Padding(
                        padding:  EdgeInsets.only(top:1.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("     "),
                            Text("Recipes",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 25,fontWeight: FontWeight.bold),),
                            Icon(Icons.search,color:Theme.of(context).textSelectionColor,size: 24 ,)
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height*0.02,),
                   Padding(
                     padding: const EdgeInsets.only(left:20.0),
                     child: Row(
                       children: <Widget>[
                         Conte(context, _data[0]['text'], _data[0]["select"],0),
                         Conte(context, _data[1]['text'], _data[1]["select"],1),
                         Conte(context, _data[2]['text'], _data[2]["select"],2),
                         Conte(context, _data[3]['text'], _data[3]["select"],3),
                       ],
                     ),
                   ),
                  ],
                ),
              ),
              Expanded(
                child: ListView.builder(
                    itemCount: recipes.length,
                    itemBuilder: (context,i){
                      return InkWell(
                          onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Recipes_Screen(
                            recipe : recipes[i],
                          ),
                        ),),
                        child: Container(
                          width: 300,
                          height: 300,
                          margin: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                          decoration: BoxDecoration(
                            color: Theme.of(context).cardColor,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: Column(
                            children: <Widget>[
                              Container(
                                width: 400,
                                height: 200,
                                child: Image.asset(recipes[i].imageUrl,fit: BoxFit.fill,),
                              ),
                              Center(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(padding: EdgeInsets.only(top: 25)),
                                    Text(recipes[i].name,style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 25,fontWeight: FontWeight.w400),),
                                    SizedBox(height: 5,),
                                    Text(recipes[i].decrep,style: TextStyle(color: Theme.of(context).highlightColor,fontSize: 16),),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    }
                ),
              ),
            ],
          ),
        ),
      )
    );
  }
}
