import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fresh2_food/Models/home_store_model.dart';
import 'package:fresh2_food/Models/items_model.dart';
import 'package:fresh2_food/Store/search_screen.dart';

class items_Home extends StatefulWidget {
  @override
  _items_HomeState createState() => _items_HomeState();
  final Home_Model home;
  final items;
  items_Home({this.home,this.items});

}

class _items_HomeState extends State<items_Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      backgroundColor: Theme.of(context).primaryColor,
      body:Container(
        child:  Column(
            children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 30),
          child: Container(
          padding: EdgeInsets.all(25),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height*0.10,
          decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(25),
                  bottomRight: Radius.circular(25)
              ),
              boxShadow: [
                BoxShadow(
                  color: Theme.of(context).backgroundColor,
                  blurRadius: 1,
                  spreadRadius: 1,
                  offset: Offset(0, 2),
                )
              ]
          ),
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              InkWell(onTap: ()=>Navigator.pop(context),child: Icon(Icons.arrow_back_ios,size: 30,color: Theme.of(context).textSelectionColor,)),
              Text(widget.home.name,style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 30,fontWeight:FontWeight.bold ),),
              Text("        "),
            ],
          ),
          ),
      ),
        ),
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: Container(
          height: MediaQuery.of(context).size.height-200,
          child:
          GridView.builder(
          shrinkWrap: true,
          itemCount: widget.items.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount( crossAxisCount: 2),
          itemBuilder: (BuildContext context, int index) {
            return InkWell(
              onTap:(){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>Search_Screen(home:widget.items[index],)));
              } ,
              child: Container(
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.all(MediaQuery.of(context).size.height*0.02),
                height: 150,
                width: 150,
                decoration: BoxDecoration(
                  color: Theme.of(context).focusColor,
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                ),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 2),
                      child: Image.asset(widget.items[index].imageUrl,height:110,width: 110,fit: BoxFit.contain,),
                    ),
                    SizedBox(height: 5,),
                    Text(widget.items[index].name,style: TextStyle(fontWeight: FontWeight.bold,color: Theme.of(context).textSelectionColor,fontSize: 20 ,),),
                  ],
                ),
              ),
            );
          }
          )
      ),
        )
          ],
        ),
      ),
    );
  }
}
