import 'package:flutter/material.dart';
import 'package:fresh2_food/Store/cart_store.dart';
import 'package:fresh2_food/Store/home_store.dart';
import 'package:fresh2_food/Store/quick_shop.dart';
import 'package:fresh2_food/Store/recipes_store.dart';
import 'file:///F:/fresh_food-master%20(2)/fresh_food-master/lib/Settings/settings.dart';

class Store extends StatefulWidget {
  @override
  _StoreState createState() => _StoreState();
}

class _StoreState extends State<Store> {
  int _currentIndex = 0;
  List _screens = [
    Home(),
    Recipes(),
    Quick_Shop(),
    Cart(),
    Settings(),
  ];
  BottomNavigationBarItem _item(IconData icon) {
    return BottomNavigationBarItem(
    icon: Icon(icon),
        title: Text('',
          style: TextStyle(fontSize: 0),
        )
    );
  }
  @override
  Widget build(BuildContext context) {
    print(_currentIndex);
    return Scaffold(
      body: _screens[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        type: BottomNavigationBarType.fixed,
        backgroundColor: Theme.of(context).primaryColor,
        selectedItemColor: Theme.of(context).accentColor,
        unselectedItemColor: Theme.of(context).textSelectionColor,
        iconSize: 30,
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
        items: [
          _item(Icons.home),
          _item(Icons.shop),
          BottomNavigationBarItem(icon: Icon(Icons.add,size: 35,),title:SizedBox.shrink(),backgroundColor: Colors.green,activeIcon: Icon(Icons.clear,size: 35,)),
          _item(Icons.shopping_cart),
          _item(Icons.settings),
        ],
      ),
    );
  }
}
