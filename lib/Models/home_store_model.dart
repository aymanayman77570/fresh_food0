import 'package:flutter/material.dart';

class Home_Model {
  String imageUrl;
  String name;
  Color color;
  Home_Model({
    this.imageUrl,
    this.name,
    this.color
  });
}


List<Home_Model> homes = [

  Home_Model(
      imageUrl: 'assets/Citrus/lime.png',
      name: 'Citrus',
      color: Colors.green[200]
  ),
  Home_Model(
      imageUrl: 'assets/banana.png',
      name: 'Tropical',
      color:  Colors.green[200]
  ),
  Home_Model(
      imageUrl: 'assets/kale.png',
      name: 'Cruciferous',
      color: Colors.green[200]
  ),
  Home_Model(
    imageUrl: 'assets/Allium/calcot.png',
    name: 'Allium',
    color:  Colors.green[200]
  ),
  Home_Model(
      imageUrl: 'assets/fruit/03.png',
      name: 'Fruit',
      color: Colors.red
  ),
  Home_Model(
      imageUrl: "assets/broccoli.png",
      name: 'Vegetables',
      color: Colors.red
  ),
];