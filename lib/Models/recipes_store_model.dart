import 'package:fresh2_food/Store/recipes_store.dart';

class Recipes_Model {
  String imageUrl;
  String name;
  String decrep;
  Recipes_Model({
    this.imageUrl,
    this.name,
    this.decrep
  });
}


List<Recipes_Model> recipes = [
  Recipes_Model(
      imageUrl: 'assets/photo.png',
      name: 'Mixed Berry Melody',
      decrep: "Berries mixed together to make a tasty dish."
  ),
  Recipes_Model(
      imageUrl: 'assets/recipe-2.png',
      name: 'Mixed Berry Melody',
      decrep: "Berries mixed together to make a tasty dish."
  ),
];