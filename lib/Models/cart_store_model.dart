class Cart_Model {
  String imageUrl;
  String name;
  String amount;
  double price;
  Cart_Model({
    this.imageUrl,
    this.name,
    this.amount,
    this.price
  });
}


List<Cart_Model> carts = [
  Cart_Model(
      imageUrl: 'assets/broccoli.png',
      name: 'Broccoli',
      amount:"2 KG",
      price: 8.80,
  ),
Cart_Model(
      imageUrl: 'assets/orange.png',
      name: 'Orange',
      amount:"1.5 KG",
      price: 7.00,
  ),
Cart_Model(
      imageUrl: 'assets/pepper.png',
      name: 'Red Peppers',
      amount:"5 KG",
      price: 1.50,
  ),
Cart_Model(
      imageUrl: 'assets/strawberry.png',
      name: 'Strawberries',
      amount:"4 KG",
      price: 22.00,
  ),
];