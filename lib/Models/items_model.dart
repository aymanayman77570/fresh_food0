
class Items_Model {
  String imageUrl;
  String name;
  Items_Model({
    this.imageUrl,
    this.name,
  });
}


List<Items_Model> fruit = [
  Items_Model(
    imageUrl: 'assets/fruit/01.png',
    name: 'Cranberry',
  ),
  Items_Model(
    imageUrl: 'assets/fruit/02.png',
    name: 'Prunus persica',
  ),
  Items_Model(
    imageUrl: 'assets/fruit/03.png',
    name: 'Ananas',
  ),
  Items_Model(
    imageUrl: 'assets/fruit/06.png',
    name: 'Mulberry',
  ),
  Items_Model(
    imageUrl: 'assets/fruit/strawberry.png',
    name: 'Strawberry',
  ),
  Items_Model(
    imageUrl: 'assets/fruit/10.png',
    name: 'Grape',
  ),
  Items_Model(
    imageUrl: 'assets/fruit/17.png',
    name: 'Watermelon',
  ),
  Items_Model(
    imageUrl: 'assets/fruit/20.png',
    name: 'Apple',
  ),
];



List<Items_Model> tropical = [
  Items_Model(
      imageUrl: 'assets/banana.png',
      name: 'Banana',
  ),
  Items_Model(
      imageUrl: 'assets/Kiwi.png',
      name: 'Kiwi',
  ),
  Items_Model(
      imageUrl: 'assets/pineapple.png',
      name: 'pineapple',
  ),
  Items_Model(
      imageUrl: 'assets/pomegranate.png',
      name: 'pomegranate',
  ),
 Items_Model(
      imageUrl: 'assets/Watermelon.png',
      name: 'Watermelon',
  ),
  Items_Model(
      imageUrl: 'assets/Mango.png',
      name: 'Mango',
  ),
];



List<Items_Model> cruci = [
  Items_Model(
    imageUrl: 'assets/Cruci/arugula.png',
    name: 'Arugula',
  ),
   Items_Model(
    imageUrl: 'assets/Cruci/broccoli.png',
    name: 'Broccoli',
  ),
   Items_Model(
    imageUrl: 'assets/Cruci/cabbage.png',
    name: 'Cabbage',
  ),
   Items_Model(
    imageUrl: 'assets/Cruci/Cauliflower.png',
    name: 'Cauliflower',
  ),
   Items_Model(
    imageUrl: 'assets/Cruci/kale.png',
    name: 'Kale',
  ),
   Items_Model(
    imageUrl: 'assets/Cruci/Turnip.png',
    name: 'Turnip',
  ),
];



List<Items_Model> citrus = [
  Items_Model(
    imageUrl: 'assets/Citrus/lemon.png',
    name: 'Lemon',
  ),Items_Model(
    imageUrl: 'assets/Citrus/lime.png',
    name: 'Lime',
  ),Items_Model(
    imageUrl: 'assets/Citrus/mandarin.png',
    name: 'Mandarin',
  ),Items_Model(
    imageUrl: 'assets/Citrus/orange.png',
    name: "Orange",
  ),



];
List<Items_Model> allium = [
  Items_Model(
    imageUrl: 'assets/Allium/calcot.png',
    name: 'Calcot',
  ),Items_Model(
    imageUrl: 'assets/Allium/Garlic.png',
    name: 'Garlic',
  ),Items_Model(
    imageUrl: 'assets/Allium/leek.png',
    name: 'Leek',
  ),Items_Model(
    imageUrl: 'assets/Allium/onion.png',
    name: 'Onion',
  ),
  Items_Model(
    imageUrl: 'assets/Allium/ramps.png',
    name: 'Ramps',
  ),
];



List<Items_Model> vegetable = [
  Items_Model(
    imageUrl: 'assets/Vegetables/bell-pepper.png',
    name: 'bell-pepper',
  ),
  Items_Model(
    imageUrl: 'assets/Vegetables/carrot.png',
    name: 'carrot',
  ), Items_Model(
    imageUrl: 'assets/Vegetables/chili.png',
    name: 'chili',
  ), Items_Model(
    imageUrl: 'assets/Vegetables/eggplant.png',
    name: 'eggplant',
  ), Items_Model(
    imageUrl: 'assets/Vegetables/pea.png',
    name: 'pea',
  ), Items_Model(
    imageUrl: 'assets/Vegetables/potato.png',
    name: 'potato',
  ), Items_Model(
    imageUrl: 'assets/Vegetables/sweet-potato.png',
    name: "sweet-potato",
  ), Items_Model(
    imageUrl: 'assets/Vegetables/tomato.png',
    name: "tomato",
  ), Items_Model(
    imageUrl: 'assets/Vegetables/zucchini.png',
    name: "zucchini",
  ),
];
