class Quick_Model {
  String imageUrl;
  String name;
  int num;
  Quick_Model({
    this.imageUrl,
    this.name,
    this.num
  });
}


List<Quick_Model> shows = [
  Quick_Model(
      imageUrl: 'assets/Quick_Images/icon-colour-apple.png',
      name: 'Apple',
      num: 0,
  ),
  Quick_Model(
    imageUrl: 'assets/Quick_Images/icon-colour-avocado.png',
    name: 'Avocado',
    num: 0,
  ), Quick_Model(
    imageUrl: 'assets/Quick_Images/icon-colour-banana.png',
    name: 'Banana',
    num: 0,
  ), Quick_Model(
    imageUrl: 'assets/Quick_Images/icon-colour-kiwi.png',
    name: 'Kiwi',
    num: 0,
  ), Quick_Model(
    imageUrl: 'assets/Quick_Images/icon-colour-lemon.png',
    name: 'Lemon',
    num: 0,
  ), Quick_Model(
    imageUrl: 'assets/Quick_Images/icon-colour-orange.png',
    name: 'Orange',
    num: 0,
  ), Quick_Model(
    imageUrl: 'assets/Quick_Images/icon-colour-peach.png',
    name: 'Peach',
    num: 0,
  ), Quick_Model(
    imageUrl: 'assets/Quick_Images/icon-colour-pear.png',
    name: 'Pear',
    num: 0,
  ), Quick_Model(
    imageUrl: 'assets/Quick_Images/icon-colour-strawberry.png',
    name: 'Strawberry',
    num: 0,
  ),
];