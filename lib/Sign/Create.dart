import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fresh2_food/Sign/Screens.dart';
import 'package:fresh2_food/Sign/Sign_in.dart';

class Create extends StatefulWidget {
  @override
  _CreateState createState() => _CreateState();
}
Widget Field(BuildContext context,bool secure,String text,IconData icon){
  return   Padding(
    padding: const EdgeInsets.only(
        left: 5,
        right: 5,
        bottom: 20
    ),
    child: TextFormField(
      keyboardType: TextInputType.text,
      obscureText: secure,
      decoration: InputDecoration(
        prefix: Padding(padding: EdgeInsets.only(left: 5)),
        contentPadding: EdgeInsets.all(20),
        prefixIcon:Icon(icon,size: 35,color:Theme.of(context).cardColor ,),
        hintText: text,
        hintStyle: TextStyle(fontSize: 16,color:Theme.of(context).textSelectionColor),
        filled: true,
        fillColor: Theme.of(context).focusColor,
        border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10)),
          borderSide: BorderSide.none,
        ),
      ),
    ),
  );
}

class _CreateState extends State<Create> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).focusColor,
      body: ListView(
        children: <Widget>[
          Container(
            color: Theme.of(context).focusColor,
            child: Column(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(35),
                          bottomRight: Radius.circular(35)
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Theme.of(context).backgroundColor,
                          blurRadius: 2,
                          spreadRadius: 2,
                          offset: Offset(5, 5),
                        )
                      ]
                  ),
                  height: MediaQuery.of(context).size.height*0.80,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: MediaQuery.of(context).size.height*0.10,
                      ),
                      Text("Create Account",style: TextStyle(color: Theme.of(context).accentColor,fontSize: 24),),
                      SizedBox(
                        height: MediaQuery.of(context).size.height*0.10,
                      ),

                      Field(context, false, "Full Name", Icons.face),
                   Padding(
                        padding: const EdgeInsets.only(
                            left: 5,
                            right: 5,
                            bottom: 20
                        ),
                        child: TextFormField(
                          keyboardType: TextInputType.text,
                          obscureText: false,
                          decoration: InputDecoration(
                            prefix: Padding(padding: EdgeInsets.only(left: 4)),
                            contentPadding: EdgeInsets.all(20),
                            prefixIcon:Icon(Icons.mail_outline,size: 35,color:Theme.of(context).cardColor,),
                            hintText: "Email",
                            hintStyle: TextStyle(fontSize: 16,color:Theme.of(context).textSelectionColor),
                            filled: true,
                            fillColor: Theme.of(context).focusColor,
                            border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10)),
                              borderSide: BorderSide.none,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 5,
                            right: 5,
                            bottom: 20
                        ),
                        child: TextFormField(
                          keyboardType: TextInputType.text,
                          obscureText: true,
                          decoration: InputDecoration(
                            prefix: Padding(padding: EdgeInsets.only(left: 4)),
                            contentPadding: EdgeInsets.all(20),
                            prefixIcon:Icon(Icons.lock_outline,size: 35,color:Theme.of(context).cardColor ,),
                            hintText: "Password",
                            hintStyle: TextStyle(fontSize: 16,color:Theme.of(context).textSelectionColor),
                            filled: true,
                            fillColor: Theme.of(context).focusColor,
                            border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10)),
                              borderSide: BorderSide.none,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height*0.10,
                      ),
                      InkWell(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder:(context)=>screens()));
                        }
                        ,child: Container(
                          width: 315,
                          height: 60,
                          decoration: BoxDecoration(
                            color: Theme.of(context).accentColor,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                          ),
                          child: Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(Icons.arrow_forward,color: Colors.white,size: 20,),
                                Text(" CREATE ACCOUNT",style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.bold),)
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height*0.07,
                ),
                InkWell(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder:(context)=>Sign_in()));
                  },
                  child: Center(
                    child: Text("SIGN IN",style: TextStyle(color: Theme.of(context).highlightColor,fontWeight: FontWeight.bold,fontSize: 18),),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
