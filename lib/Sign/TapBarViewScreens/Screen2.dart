import 'package:flutter/material.dart';

class Screen2 extends StatefulWidget {
  @override
  _Screen2State createState() => _Screen2State();
}

class _Screen2State extends State<Screen2> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: MediaQuery.of(context).size.height*0.05,
          ),
          Container(child: Image.asset("assets/screen.png",),width: 300,height: 300,color: Theme.of(context).primaryColor,),
          SizedBox(
            height: MediaQuery.of(context).size.height*0.10,
          ),
          Text(" With one click you can add every \n ingredient for a recipe to your cart",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 18),)
        ],
      ),
    );
  }
}
