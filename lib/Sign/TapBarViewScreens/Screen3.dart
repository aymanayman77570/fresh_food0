import 'package:flutter/material.dart';

class Screen3 extends StatefulWidget {
  @override
  _Screen3State createState() => _Screen3State();
}
class _Screen3State extends State<Screen3> {
  bool isSwitchedAll=false;
  bool isSwitched1=false;
  bool isSwitched2=false;
  bool isSwitched3=false;
  bool isSwitched4=false;
  bool isSwitched5=false;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: <Widget>[
          Center(child: Text("Recipe Preferences",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 20),)),
          SizedBox(
            height: MediaQuery.of(context).size.height*0.05,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("All",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 18)),
              Switch(
                activeColor: Theme.of(context).accentColor,
                inactiveThumbColor: Theme.of ( context ).textSelectionColor ,
                inactiveTrackColor:  Theme.of ( context ).textSelectionColor,
                activeTrackColor: Theme.of(context).accentColor,
                value: isSwitchedAll,
                onChanged: (value) {
                  setState(() {
                    isSwitchedAll = value;
                    isSwitched1=isSwitchedAll;
                    isSwitched2=isSwitchedAll;
                    isSwitched3=isSwitchedAll;
                    isSwitched4=isSwitchedAll;
                    isSwitched5=isSwitchedAll;
                  });
                },
              ),
            ],
          ),

          SizedBox(
            height: MediaQuery.of(context).size.height*0.01,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Vegan",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 18)),
              Switch(
                activeColor: Theme.of(context).accentColor,
                inactiveThumbColor: Theme.of ( context ).textSelectionColor ,
                inactiveTrackColor:  Theme.of ( context ).textSelectionColor,
                activeTrackColor: Theme.of(context).accentColor,
                value: isSwitched1,

                onChanged: (value) {
                  setState(() {
                    isSwitched1 = value;

                  });
                },
              ),
            ],
          ),

          SizedBox(
            height: MediaQuery.of(context).size.height*0.01,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Vegetarian",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 18)),
              Switch(
                activeColor: Theme.of(context).accentColor,
                inactiveThumbColor: Theme.of ( context ).textSelectionColor ,
                inactiveTrackColor:  Theme.of ( context ).textSelectionColor,
                activeTrackColor: Theme.of(context).accentColor,
                value: isSwitched2,

                onChanged: (value) {
                  setState(() {
                    isSwitched2 = value;

                  });
                },
              ),
            ],
          ),

          SizedBox(
            height: MediaQuery.of(context).size.height*0.01,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Paleo",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 18)),
              Switch(
                activeColor: Theme.of(context).accentColor,
                inactiveThumbColor: Theme.of ( context ).textSelectionColor ,
                inactiveTrackColor:  Theme.of ( context ).textSelectionColor,
                activeTrackColor: Theme.of(context).accentColor,
                value: isSwitched3,

                onChanged: (value) {
                  setState(() {
                    isSwitched3 = value;

                  });
                },
              ),
            ],
          ),

          SizedBox(
            height: MediaQuery.of(context).size.height*0.01,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Keto",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 18)),
              Switch(
                activeColor: Theme.of(context).accentColor,
                inactiveThumbColor: Theme.of ( context ).textSelectionColor ,
                inactiveTrackColor:  Theme.of ( context ).textSelectionColor,
                activeTrackColor: Theme.of(context).accentColor,
                value: isSwitched4,

                onChanged: (value) {
                  setState(() {
                    isSwitched4 = value;

                  });
                },
              ),
            ],
          ),

          SizedBox(
            height: MediaQuery.of(context).size.height*0.01,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Low Carb",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 18)),
              Switch(
                activeColor: Theme.of(context).accentColor,
                inactiveThumbColor: Theme.of ( context ).textSelectionColor ,
                inactiveTrackColor:  Theme.of ( context ).textSelectionColor,
                activeTrackColor: Theme.of(context).accentColor,
                value: isSwitched5,

                onChanged: (value) {
                  setState(() {
                    isSwitched5 = value;

                  });
                },
              ),
            ],
          ),

          SizedBox(
            height: MediaQuery.of(context).size.height*0.05,
          ),
          Text("Tailor your Recipes feed exactly\n              "
              "how you like it",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 18),)

        ],
      ),
    );
  }
}
