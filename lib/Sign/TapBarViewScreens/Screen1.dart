import 'package:flutter/material.dart';
import 'package:fresh2_food/main.dart';

class Screen1 extends StatefulWidget {
  @override
  _Screen1State createState() => _Screen1State();
}

class _Screen1State extends State<Screen1> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: MediaQuery.of(context).size.height*0.05,
          ),
          Container(child: Image.asset("assets/screen1.png",),width: 300,height: 300,color: Theme.of(context).primaryColor,),
          SizedBox(
            height: MediaQuery.of(context).size.height*0.10,
          ),
          Text(" Quickly search and add \nhealthy foods to your cart",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 18),)
        ],
      ),
    );
  }
}
