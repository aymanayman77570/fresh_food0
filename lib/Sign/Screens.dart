import 'package:flutter/material.dart';
import 'package:fresh2_food/Sign/TapBarViewScreens/Screen1.dart';
import 'package:fresh2_food/Sign/TapBarViewScreens/Screen2.dart';
import 'package:fresh2_food/Sign/TapBarViewScreens/Screen3.dart';
import 'package:fresh2_food/Store/home_store.dart';
import 'package:fresh2_food/Store/store.dart';

class screens extends StatefulWidget {
  @override
  _screensState createState() => _screensState();
}

var widget=<Widget>[
  Screen1(),
  Screen2(),
  Screen3(),
];

class _screensState extends State<screens> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DefaultTabController(
        length:widget.length,
        child: Container (
              color: Theme
                  .of ( context )
                  .focusColor ,
              child: Column (
                children: <Widget>[
                  Container (
                    decoration: BoxDecoration (
                        color: Theme
                            .of ( context )
                            .primaryColor ,
                        borderRadius: BorderRadius.only (
                            bottomLeft: Radius.circular ( 35 ) ,
                            bottomRight: Radius.circular ( 35 )
                        ) ,
                        boxShadow: [
                          BoxShadow (
                            color: Theme.of(context).backgroundColor ,
                            blurRadius: 2 ,
                            spreadRadius: 2 ,
                            offset: Offset ( 5 , 5 ) ,
                          )
                        ]
                    ) ,
                    height: MediaQuery
                        .of ( context )
                        .size
                        .height * 0.87 ,
                    width: MediaQuery
                        .of ( context )
                        .size
                        .width ,
                    padding: EdgeInsets.all ( 10 ) ,
                    child: Column (
                      children: <Widget>[
                        SizedBox (
                          height: MediaQuery
                              .of ( context )
                              .size
                              .height * 0.07 ,
                        ) ,
                        Container(
                          width: 400,
                          height: 510,
                          child: TabBarView(children:widget ),
                        ),
                        SizedBox (
                          height: MediaQuery
                              .of ( context )
                              .size
                              .height * 0.03 ,
                        ) ,
                        TabPageSelector(color: Theme.of(context).focusColor,selectedColor: Theme.of(context).textSelectionColor,),
                      ] ,
                    ) ,
                  ) ,
                  SizedBox (
                    height: MediaQuery
                        .of ( context )
                        .size
                        .height * 0.05 ,
                  ) ,
                  InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder:(context)=>Store()));
                    },
                    child: Center(
                      child: Text("Skip",style: TextStyle(color: Theme.of(context).textSelectionColor,fontWeight: FontWeight.bold,fontSize: 18),),
                    ),
                  ),
                ] ,
              )
            )
        ),
    );
  }
}