import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fresh2_food/Sign/Create.dart';
import 'package:fresh2_food/Store/store.dart';

class Sign_in extends StatefulWidget {
  @override
  _Sign_inState createState() => _Sign_inState();
}

class _Sign_inState extends State<Sign_in> {
  Widget Field(BuildContext context,bool secure,String text,IconData icon){
    return Padding(
      padding: const EdgeInsets.only(
          left: 5,
          right: 5,
          bottom: 20
      ),
      child: TextFormField(
        keyboardType: TextInputType.text,
        obscureText: secure,
        decoration: InputDecoration(
          prefix: Padding(padding: EdgeInsets.only(left: 5)),
          contentPadding: EdgeInsets.all(20),
          prefixIcon:Icon(icon,size: 35,color:Theme.of(context).cardColor ,),
          hintText: text,
          hintStyle: TextStyle(fontSize: 16,color:Theme.of(context).textSelectionColor),
          filled: true,
          fillColor: Theme.of(context).focusColor,
          border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10)),
            borderSide: BorderSide.none,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    String _email;
    String _password;
    GlobalKey<FormState> _globalKey = GlobalKey<FormState>();
    return Scaffold(
      backgroundColor: Theme.of(context).focusColor,
      body: ListView(
        children: <Widget>[
          Container(
            color: Theme.of(context).focusColor,
            child: Column(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(35),
                          bottomRight: Radius.circular(35)
                      ),
                    boxShadow: [
                      BoxShadow(
                        color: Theme.of(context).backgroundColor,
                        blurRadius: 1,
                        spreadRadius: 2,
                        offset: Offset(0, 5),
                      )
                    ]
                  ),
                  height: MediaQuery.of(context).size.height*0.80,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: MediaQuery.of(context).size.height*0.12,
                      ),
                      Text("Sign In",style: TextStyle(color: Theme.of(context).accentColor,fontSize: 24),),
                      SizedBox(
                        height: MediaQuery.of(context).size.height*0.12,
                      ),
                      Form(
                        key: _globalKey,
                        child: Column(
                          children: <Widget>[
                            Field(context, false, "Email", Icons.mail_outline,

                            ),
                            Field(context, true, "Password", Icons.lock_outline),
                          ],
                        ),
                      ),
                      Center(
                        child: Text("Forget Password?",style: TextStyle(color:Theme.of(context).highlightColor,fontSize: 16),),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height*0.12,
                      ),
                      InkWell(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder:(context)=>Store()));
                        },
                        child: Container(
                          width: 315,
                          height: 60,
                          decoration: BoxDecoration(
                            color: Theme.of(context).accentColor,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                          ),
                          child: Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(Icons.arrow_forward,color: Colors.white,size: 20,),
                                Text(" SIGN IN",style: TextStyle(color:Colors.white,fontSize: 20,fontWeight: FontWeight.bold),)
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height*0.07,
                ),
                InkWell(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder:(context)=>Create()));
                  },
                  child: Center(
                    child: Text("CREATE ACCOUNT",style: TextStyle(color: Theme.of(context).textSelectionColor,fontWeight: FontWeight.bold,fontSize: 18),),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
