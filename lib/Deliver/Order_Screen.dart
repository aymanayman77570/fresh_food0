import 'dart:async';
import 'package:flutter/material.dart';
import 'package:fresh2_food/Store/home_store.dart';
import 'package:fresh2_food/Store/store.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'file:///F:/fresh_food-master%20(2)/fresh_food-master/lib/Settings/AppState.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: 2),
            () => Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (BuildContext context) => Store())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:Theme.of(context).primaryColor,
      body: Center(
        child: Container(
          color: Theme.of(context).primaryColor,
          child: Column(
            children: <Widget>[
              SizedBox(height: 120,),
              Container(
                  height: 300,
                  width: 300,
                  child: Lottie.asset('assets/accepted.json' )),
              Text("    ORDER PLACED.\nYou order number is",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 22),),
              Text("#12345",style: TextStyle(color: Theme.of(context).accentColor,fontSize: 26),),
            ],
          ),
        ),
      ),
    );
  }
}