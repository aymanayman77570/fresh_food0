import 'package:flutter/material.dart';
import 'package:fresh2_food/Deliver/Deliver_address.dart';
import 'package:fresh2_food/Deliver/Deliver_order.dart';
import 'package:fresh2_food/Deliver/Deliver_payment.dart';
import 'package:fresh2_food/Deliver/Delivery_options.dart';
import 'package:fresh2_food/Deliver/Order_Screen.dart';

class Deliver_Home extends StatefulWidget {
  @override
  _Deliver_HomeState createState() => _Deliver_HomeState();
}
class _Deliver_HomeState extends State<Deliver_Home> {

  static List<Widget> screen=[
    Deliver_Address(),
    Deliver_Options(),
    Deliver_Payment(),
    Deliver_Order(),
  ];

  int i =0;
  List<Map<String,dynamic>> _data=[
    {
      "text":"Delivery Address",
      'select': true,
      "check":false
    }  ,
    {
      "text":"Delivery Options",
      'select': false,
      "check":false
    }  ,
    {
      "text":"Delivery Methods",
      'select': false,
      "check":false
    }  ,
    {
      "text":"Order Summary",
      'select': false,
      "check":false
    }  ,
  ];
  Widget Conte(BuildContext context,bool select,IconData icon,int i){
    return Container(
      width: 80,
      margin: EdgeInsets.symmetric(horizontal: 2.5),
      height: 45,
      decoration: BoxDecoration(
        color: _data[i]["select"]?Theme.of(context).accentColor:Theme.of(context).primaryColor,
        borderRadius: BorderRadius.all(Radius.circular(30)),
      ),
      child: Center(
        child: Icon(icon,color:_data[i]["check"]? Theme.of(context).accentColor:_data[i]["select"]?Theme.of(context).primaryColor:Theme.of(context).textSelectionColor,size: 38,),
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          color:Theme.of(context).primaryColor,
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: ListView(
            children: <Widget>[
              Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(15),
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height*0.20,
                      decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(25),
                              bottomRight: Radius.circular(25)
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Theme.of(context).backgroundColor,
                              blurRadius: 1,
                              spreadRadius: 1,
                              offset: Offset(0, 5),
                            )
                          ]
                      ),
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              InkWell(onTap: (){
                                setState(() {
                                  if(i==0)
                                  {
                                    Navigator.pop(context);
                                  }
                                  _data[i]["select"]=false;
                                  _data[i]["check"]=false;
                                  i>0?i--:i;
                                  _data[i]["select"]=true;
                                  _data[i]["check"]=false;
                                });
                              },
                                  child: Icon(Icons.arrow_back,size: 25,color: Theme.of(context).textSelectionColor,)),
                              Text(_data[i]["text"],style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 22),),
                              Text(""),
                            ],
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height*0.045,),
                          Row(
                            children: <Widget>[
                              SizedBox(width: 5,),
                              Conte(context, true,Icons.place,0),
                              SizedBox(width: 5,),
                              Conte(context, false,Icons.train,1),
                              SizedBox(width: 5,),
                              Conte(context, false,Icons.credit_card,2),
                              SizedBox(width: 5,),
                              Conte(context, false,Icons.menu,3),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      height: 485,
                      child: screen[i],
                    ),
                    i<=2?InkWell(
                      onTap: (){
                        setState(() {
                          _data[i]["select"]=false;
                          _data[i]["check"]=true;
                          i++;
                          _data[i]["select"]=true;
                        });
                      },
                      child: Container(
                        width: 315,
                        height: 60,
                        decoration: BoxDecoration(
                          color: Theme.of(context).accentColor,
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                        ),
                        child: Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(Icons.arrow_forward,color: Colors.white,size: 20,),
                              Text(" Continue",style: TextStyle(color:Colors.white,fontSize: 20,fontWeight: FontWeight.bold),)
                            ],
                          ),
                        ),
                      ),
                    ): InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute( builder: (BuildContext context) =>Splash()));
                      },
                      child: Container(
                        width: 315,
                        height: 60,
                        decoration: BoxDecoration(
                          color: Theme.of(context).accentColor,
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                        ),
                        child: Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(Icons.arrow_forward,color: Colors.white,size: 20,),
                              Text(" PLACE ORDER",style: TextStyle(color:Colors.white,fontSize: 20,fontWeight: FontWeight.bold),)
                            ],
                          ),
                        ),
                      ),
                    ),
                  ]
              ),
            ],
          )
      ),
    );
  }
}
