import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Deliver_Options extends StatefulWidget {
  @override
  _Deliver_OptionsState createState() => _Deliver_OptionsState();
}
bool check1=true;
bool check2=false;
class _Deliver_OptionsState extends State<Deliver_Options> {

  int selected=0;
  List<Map<String,dynamic>> _data=[
    {
      "text":"14 Oct",
      'select': true
    },
    {
      "text":"15 Oct",
      'select': false
    } ,
    {
      "text":"16 Oct",
      'select': false
    } ,
    {
      "text":"17 Oct",
      'select': false
    } ,
    {
      "text":"18 Oct",
      'select': false
    } ,
  ];
  int selected1=0;
  List<Map<String,dynamic>> _data1=[
    {
      "text":"8:00 am",
      'select': true
    },
    {
      "text":"9:00 am",
      'select': false
    } ,
    {
      "text":"10:00 am",
      'select': false
    } ,
    {
      "text":"11:00 am",
      'select': false
    } ,
    {
      "text":"12:00 am",
      'select': false
    } ,
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Container(
        padding: EdgeInsets.all(15),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 200),
              child: Text("Select Speed",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 22),),
            ),
            SizedBox(height: 15,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(10),
                    height: 140,
                    width: 150,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10),
                        ),
                        color: Theme.of(context).focusColor
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Icon(Icons.directions_car,color:check1?Theme.of(context).accentColor: Theme.of(context).highlightColor,size: 35,),
                        Column(
                          children: <Widget>[
                            Text("Standard",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 20),),
                            Text("2-3 days (free)",style: TextStyle(color: Theme.of(context).highlightColor,fontSize: 15),),
                          ],
                        ),
                        InkWell(
                          onTap: (){
                            setState(() {
                              check1=true;
                              check2=false;
                            });
                          },
                          child: Container(
                            height: 25,
                            width: 25,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(50)),
                                color:check1? Theme.of(context).accentColor:Theme.of(context).cardColor
                            ),
                            child: Center(
                              child: Icon(Icons.check,color: check1?Theme.of(context).primaryColor:Theme.of(context).cardColor,),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    height: 140,
                    width: 150,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10),
                        ),
                        color: Theme.of(context).focusColor
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Icon(Icons.local_car_wash,color:check2?Theme.of(context).accentColor: Theme.of(context).highlightColor,size: 35,),
                        Column(
                          children: <Widget>[
                            Text("Supersonic",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 20),),
                            Text("Same day (£4.5)",style: TextStyle(color: Theme.of(context).highlightColor,fontSize: 15),),
                          ],
                        ),
                        InkWell(
                          onTap: (){
                            setState(() {
                              check2=true;
                              check1=false;
                            });
                          },
                          child: Container(
                            height: 25,
                            width: 25,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(50)),
                                color:check2? Theme.of(context).accentColor:Theme.of(context).cardColor
                            ),
                            child: Center(
                              child: Icon(Icons.check,color: check2?Theme.of(context).primaryColor:Theme.of(context).cardColor,),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 15,),
            Padding(
              padding: const EdgeInsets.only(right: 210),
              child: Text("Select Date",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 22),),
            ),
            SizedBox(height: 15,),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 60,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                  itemCount: _data.length,
                  itemBuilder:(context,i){
                    return  InkWell(
                      onTap: (){
                        setState(() {
                          _data[selected]["select"]=false;
                          selected=i;
                          _data[selected]["select"]=true;
                        });
                      },
                      child: Container(
                        width: 100,
                        margin: EdgeInsets.symmetric(horizontal: 5),
                        height: 80,
                        decoration: BoxDecoration(
                          color: _data[i]["select"]?Theme.of(context).accentColor:Theme.of(context).focusColor,
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                        ),
                        child: Center(
                          child: Text(_data[i]["text"],style: TextStyle(color: _data[i]["select"]?Theme.of(context).primaryColor:Theme.of(context).highlightColor,fontSize: 22),),
                        ),
                      ),
                    );
              }
              ),
            ),     SizedBox(height: 15,),
            Padding(
              padding: const EdgeInsets.only(right: 210),
              child: Text("Select Time",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 22),),
            ),
            SizedBox(height: 15,),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 60,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                  itemCount: _data1.length,
                  itemBuilder:(context,i){
                    return  InkWell(
                      onTap: (){
                        setState(() {
                          _data1[selected1]["select"]=false;
                          selected1=i;
                          _data1[selected1]["select"]=true;
                        });
                      },
                      child: Container(
                        width: 100,
                        margin: EdgeInsets.symmetric(horizontal: 5),
                        height: 80,
                        decoration: BoxDecoration(
                          color: _data1[i]["select"]?Theme.of(context).accentColor:Theme.of(context).focusColor,
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                        ),
                        child: Center(
                          child: Text(_data1[i]["text"],style: TextStyle(color: _data1[i]["select"]?Theme.of(context).primaryColor:Theme.of(context).highlightColor,fontSize: 22),),
                        ),
                      ),
                    );
              }
              ),
            ),
            SizedBox(height: 25,),
          ],
        ),
      ),
    );
  }
}
