import "package:flutter/material.dart";
import 'package:fresh2_food/Deliver/Delivery_options.dart';
import 'package:fresh2_food/Models/cart_store_model.dart';

class Deliver_Order extends StatefulWidget {
  @override
  _Deliver_OrderState createState() => _Deliver_OrderState();
}
Widget Field(BuildContext context,String text){
  return Padding(
    padding: const EdgeInsets.only(
        left: 5,
        right: 5,
        bottom: 20
    ),
    child: TextFormField(
      keyboardType: TextInputType.text,
      style: TextStyle(color: Theme.of(context).textSelectionColor),
      decoration: InputDecoration(

        contentPadding: EdgeInsets.only(left:0),
        hintText: text,
        hintStyle: TextStyle(fontSize: 18,color:Theme.of(context).textSelectionColor),
        filled: true,
        fillColor: Theme.of(context).cardColor,
        border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10)),
          borderSide: BorderSide.none,
        ),
      ),
    ),
  );
}
class _Deliver_OrderState extends State<Deliver_Order> {
  double Prices=9.30;
  bool check=false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Container(
        color: Theme.of(context).primaryColor,
        child: Column(
          children: <Widget>[
            Container(
              height: 270,
              child: ListView.builder(
                  itemCount: carts.length,
                  itemBuilder:(context,i){
                    if(Prices==0){
                      for(int n=0;n<carts.length;n++){
                        Prices+=carts[n].price;
                      }
                    }
                    return Container (
                      width: 315 ,
                      height: 55 ,
                      margin: EdgeInsets.symmetric(horizontal: 20,vertical: 6) ,
                      padding: EdgeInsets.all ( 10 ) ,
                      decoration: BoxDecoration (
                          borderRadius: BorderRadius.all ( Radius.circular ( 10 ) ) ,
                          color: Theme
                              .of ( context )
                              .cardColor
                      ) ,
                      child: Center (
                        child: Row (
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row (
                              children: <Widget>[
                                Image.asset(carts[i].imageUrl,height: 120,width: 50,fit: BoxFit.contain,),
                                SizedBox(width: 7,),
                                Text ( carts[i].name , style: TextStyle ( color: Theme
                                    .of ( context )
                                    .textSelectionColor , fontSize: 18 ) , ) ,
                              ] ,
                            ) ,
                            Row(
                              children: <Widget>[
                                Text ( carts[i].amount , style: TextStyle ( color: Theme.of ( context ).highlightColor , fontSize: 18 ) , ) ,
                                SizedBox(width: 10,),
                                Text ("£"+carts[i].price.toString() , style: TextStyle ( color: Theme.of ( context ).textSelectionColor , fontSize: 18 ) , ) ,
                                SizedBox(width: 10,),
                              ],
                            ),
                          ] ,
                        ) ,
                      ) ,
                    ) ;
                  }
              ),
            ),
            SizedBox(height: 20,),
            InkWell(
              onTap: (){
                setState(() {
                  check=!check;
                });
              }
            ,child: Container (
                width: 355 ,
                height: 60 ,
                margin: EdgeInsets.symmetric(horizontal: 20,vertical: 6) ,
                padding: EdgeInsets.only(left: 25,top: 20) ,
                decoration: BoxDecoration (
                    borderRadius: BorderRadius.all ( Radius.circular ( 10 ) ) ,
                    color: Theme
                        .of ( context )
                        .cardColor
                ) ,
                child:check?Field(context, "Enter Coupon"):Text("Coupon Code",style: TextStyle(color: Theme.of(context).highlightColor,fontSize: 20),)
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Sub-total",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 15),),
                      Text("£"+Prices.toString(),style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 15),)
                    ],
                  ),
                  SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Delivery",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 15),),
                      Text(check2?'4.5':"Standard(free)",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 15),)
                    ],
                  ),
                  SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Total",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 25),),
                      Text("£"+(check2?(Prices+4.5).toString():Prices.toString()),style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 25),)
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      )
    );
  }
}
