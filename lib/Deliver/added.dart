import 'dart:async';
import 'package:flutter/material.dart';
import 'package:fresh2_food/Store/home_store.dart';
import 'package:fresh2_food/Store/store.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'file:///F:/fresh_food-master%20(2)/fresh_food-master/lib/Settings/AppState.dart';

class Splashadd extends StatefulWidget {
  @override
  _SplashaddState createState() => _SplashaddState();
}

class _SplashaddState extends State<Splashadd> {
  @override
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: 3),
            () => Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (BuildContext context) => Store())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:Theme.of(context).primaryColor,
      body: Center(
        child: Container(
          color: Theme.of(context).primaryColor,
          child: Column(
            children: <Widget>[
              SizedBox(height: 120,),
              Padding(
                padding: const EdgeInsets.only(right: 35),
                child: Container(
                    height: 500,
                    width: 500,
                    child: Lottie.asset('assets/card.json' )),
              ),
              Text("Added to Card",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 22),),

            ],
          ),
        ),
      ),
    );
  }
}