import 'package:flutter/material.dart';
import 'package:fresh2_food/Deliver/deliver_home.dart';
import 'package:fresh2_food/Sign/Screens.dart';

class Deliver_Address extends StatefulWidget {
  @override
  _Deliver_AddressState createState() => _Deliver_AddressState();
}
Widget Field(BuildContext context,String text){
  return Padding(
    padding: const EdgeInsets.only(
        left: 5,
        right: 5,
        bottom: 20
    ),
    child: TextFormField(
      keyboardType: TextInputType.text,
      style: TextStyle(color: Theme.of(context).textSelectionColor),
      decoration: InputDecoration(
        prefix: Padding(padding: EdgeInsets.only(left: 7)),
        contentPadding: EdgeInsets.all(20),
        hintText: text,
        hintStyle: TextStyle(fontSize: 18,color:Theme.of(context).textSelectionColor),
        filled: true,
        fillColor: Theme.of(context).focusColor,
        border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10)),
          borderSide: BorderSide.none,
        ),
      ),
    ),
  );
}

class _Deliver_AddressState extends State<Deliver_Address> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(20),
        color: Theme.of(context).primaryColor,
        child:Column(
          children: <Widget>[
            Field(context, "Full Name"),
            Field(context,  "Town/City"),
            Field(context,  "Street Address"),
            SizedBox(height: 180,),
          ],
        ),
      ),
    );
  }
}
