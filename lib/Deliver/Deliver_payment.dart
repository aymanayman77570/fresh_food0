import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Deliver_Payment extends StatefulWidget {
  @override
  _Deliver_PaymentState createState() => _Deliver_PaymentState();
}

class _Deliver_PaymentState extends State<Deliver_Payment> {
  Widget Field(BuildContext context,String text,TextInputType type,IconData icon){
    return Padding(
      padding: const EdgeInsets.only(
          left: 5,
          right: 5,
          bottom: 15
      ),
      child: TextFormField(
        keyboardType: type,
        style: TextStyle(color: Theme.of(context).textSelectionColor),
        decoration: InputDecoration(
          prefix: Padding(padding: EdgeInsets.only(left: 7)),
          contentPadding: EdgeInsets.all(20),
          hintText: text,
          hintStyle: TextStyle(fontSize: 18,color:Theme.of(context).highlightColor),
          filled: true,
          prefixIcon: Icon(icon,color: Theme.of(context).highlightColor,size: 30,),
          fillColor: Theme.of(context).focusColor,
          border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10)),
            borderSide: BorderSide.none,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body:ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(10),
            child: Center(
              child: Column(
                children: <Widget>[
                  SizedBox(height: 10,),
                  Field(context, "Cardholder Name",TextInputType.text,Icons.person_outline),
                  Field(context, "Card Number",TextInputType.number,Icons.credit_card),
                  Field(context, "Expiry Date",TextInputType.datetime,Icons.calendar_today),
                  Field(context, "Security Code",TextInputType.number,Icons.security),
                ],
              ),
            ),
          ),
        ],
      ) ,
    );
  }
}
