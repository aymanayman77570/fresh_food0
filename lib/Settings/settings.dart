import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'file:///F:/fresh_food-master%20(2)/fresh_food-master/lib/Settings/AppState.dart';
import 'file:///F:/fresh_food-master%20(2)/fresh_food-master/lib/Settings/Live_Chat.dart';
import 'package:fresh2_food/Splash_screen.dart';
import 'file:///F:/fresh_food-master%20(2)/fresh_food-master/lib/Settings/store_settings_account.dart';
import 'file:///F:/fresh_food-master%20(2)/fresh_food-master/lib/Settings/Your_order.dart';
import 'package:provider/provider.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}
Widget Containe(BuildContext context,IconData icon,String text){
  return  Container(
    width:340,
    height: 65,
    margin: EdgeInsets.all(3),
    padding: EdgeInsets.all(10),
    decoration: BoxDecoration(
        borderRadius:BorderRadius.all(Radius.circular(10)),
        color: Theme.of(context).focusColor
    ),
    child: Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              Icon(icon,color: Theme.of(context).accentColor,size: 30,),
              SizedBox(width: 12,),
              Text(text,style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 20),),
            ],
          ),
          Icon(Icons.keyboard_arrow_right,color: Theme.of(context).textSelectionColor,size: 26,)
        ],
      ),
    ),
  );
}
class _SettingsState extends State<Settings>with SingleTickerProviderStateMixin {
  bool isChecked = false;
  @override
  Widget build (BuildContext context) {
    return Scaffold (
      backgroundColor: Theme
          .of ( context )
          .primaryColor ,
      body: Column (
        children: <Widget>[
          Container (
            padding: EdgeInsets.all ( 25 ) ,
            width: MediaQuery
                .of ( context )
                .size
                .width ,
            height: MediaQuery
                .of ( context )
                .size
                .height * 0.15 ,
            decoration: BoxDecoration (
              color: Theme
                  .of ( context )
                  .primaryColor ,
              borderRadius: BorderRadius.only (
                  bottomLeft: Radius.circular ( 25 ) ,
                  bottomRight: Radius.circular ( 25 )
              ) ,
              boxShadow: [
                BoxShadow (
                  color: Theme.of(context).backgroundColor,
                  blurRadius: 3 ,
                  spreadRadius: 1 ,
                  offset: Offset ( 0 , 2 ) ,
                )
              ] ,
            ) ,
            child: Center(
              child: Padding(
                padding: const EdgeInsets.only(top:15.0),
                child: Text (
                  "Settings" , style: TextStyle ( fontSize: 25 , color: Theme.of ( context ).textSelectionColor ,fontWeight: FontWeight.bold) , ),
              ),
            ) ,
          ) ,
          SizedBox ( height: MediaQuery
              .of ( context )
              .size
              .height * 0.04 , ) ,
          InkWell(onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Account_Screen())),child: Containe ( context , Icons.person_outline , "Your Account" )) ,
          InkWell(onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Orders())),child: Containe ( context , Icons.menu , "Your Orders" )) ,
          InkWell(onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Chat())),child: Containe ( context , Icons.chat , "Live Chat" )) ,
          Container (
            width: 340 ,
            height: 65 ,
            margin: EdgeInsets.all ( 3 ) ,
            padding: EdgeInsets.all ( 10 ) ,
            decoration: BoxDecoration (
                borderRadius: BorderRadius.all ( Radius.circular ( 10 ) ) ,
                color: Theme
                    .of ( context )
                    .focusColor
            ) ,
            child: Center (
              child: Row (
                mainAxisAlignment: MainAxisAlignment.spaceBetween ,
                children: <Widget>[
                  Row (
                    children: <Widget>[
                      Icon ( Icons.lightbulb_outline , color: Theme
                          .of ( context )
                          .accentColor , size: 30 , ) ,
                      SizedBox ( width: 12 , ) ,
                      Text ( "Dark Mode" , style: TextStyle ( color: Theme.of ( context ).textSelectionColor , fontSize: 20 ) , ) ,
                    ] ,
                  ) ,
                  Switch(
                    activeColor: Theme.of(context).accentColor,
                    inactiveThumbColor: Theme.of ( context ).textSelectionColor ,
                    inactiveTrackColor:  Theme.of ( context ).textSelectionColor,
                    activeTrackColor: Theme.of(context).accentColor,
                    value: isChecked,
                    onChanged: (value) {
                      setState(() {
                        isChecked = value;
                        Provider.of<AppStateNotifier>(context,listen: false).isDarkMode;
                        Provider.of<AppStateNotifier>(context,listen: false).updateTheme(isChecked);
                      });
                    },
                  ),
                ] ,
              ) ,
            ) ,
          ) ,
          SizedBox ( height: 200 , ) ,
          InkWell (
            onTap: () {
              Navigator.push ( context ,
                  MaterialPageRoute ( builder: (context) => SplashScreen ( ) ) );
            } ,
            child: Container (
              width: 340 ,
              height: 65 ,
              margin: EdgeInsets.all ( 3 ) ,
              padding: EdgeInsets.all ( 10 ) ,
              decoration: BoxDecoration (
                  borderRadius: BorderRadius.all ( Radius.circular ( 10 ) ) ,
                  color: Theme
                      .of ( context )
                      .focusColor
              ) ,
              child: Center (
                child: Row (
                  children: <Widget>[
                    Row (
                      children: <Widget>[
                        Icon ( Icons.close , color: Theme
                            .of ( context )
                            .accentColor , size: 30 , ) ,
                        SizedBox ( width: 12 , ) ,
                        Text ( "Sign Out" , style: TextStyle ( color: Theme
                            .of ( context )
                            .textSelectionColor , fontSize: 20 ) , ) ,
                      ] ,
                    ) ,
                  ] ,
                ) ,
              ) ,
            ) ,
          ) ,
        ] ,
      ) ,
    );
  }
}
