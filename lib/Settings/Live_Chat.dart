
import 'file:///F:/fresh_food-master%20(2)/fresh_food-master/lib/Settings/settings.dart';
import 'package:flutter/material.dart';


class Chat extends StatefulWidget {
  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
         body: Container(
          child:  Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(30),
                  width: MediaQuery.of(context).size.width,
                   height: MediaQuery.of(context).size.height/7,
                   decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.only(
                     bottomLeft: Radius.circular(25),
                  bottomRight: Radius.circular(25)
                        ),
                     boxShadow: [
                    BoxShadow(
                    color: Theme.of(context).backgroundColor,
                      blurRadius: 1,
                      spreadRadius: 1,
                      offset: Offset(0, 2),
                            )
               ]
           ),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                             InkWell(onTap:(){
                               Navigator.pop(context, MaterialPageRoute( builder: (BuildContext context) =>Settings()));
                          },
                                child: Icon(Icons.arrow_back_ios,size: 25,color: Theme.of(context).textSelectionColor ,)
                          ),
                          Text("Live Chat",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize:25,fontWeight: FontWeight.bold)),
                              Text("      ")
                     ]
                      ),
             ),

                Expanded(
                  child:Container()
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                      margin: EdgeInsets.symmetric(horizontal: 5.0),
                      height: 60,
                      width: 370,
                      child: TextFormField(
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          prefix: Padding(padding: EdgeInsets.only(left: 5)),
                          suffixIcon: Icon(Icons.keyboard_voice,color:Theme.of(context).textSelectionColor ,),
                          contentPadding: EdgeInsets.all(20),
                          hintText: "Type Here ..",
                          hintStyle: TextStyle(fontSize: 16,color:Theme.of(context).textSelectionColor),
                          filled: true,
                          fillColor: Theme.of(context).focusColor,
                          border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10)),
                            borderSide: BorderSide.none,
                          ),
                        ),
                      ),
                    ),
                    ]
                ),

    ]
    )
    )
    );
  }
}

