import 'package:fresh2_food/Store/cart_store.dart';
import 'file:///F:/fresh_food-master%20(2)/fresh_food-master/lib/Settings/settings.dart';
import 'package:flutter/material.dart';
import 'package:fresh2_food/Store/home_store.dart';


class Orders extends StatefulWidget {
  @override
  _OrdersState createState() => _OrdersState();
}

class _OrdersState extends State<Orders> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Container(
        child:  Column(
            children: <Widget>[
        Container(
        padding: EdgeInsets.all(30),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height*0.18,
        decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(25),
                bottomRight: Radius.circular(25)
            ),
            boxShadow: [
              BoxShadow(
                color: Theme.of(context).backgroundColor,
                blurRadius: 1,
                spreadRadius: 1,
                offset: Offset(0, 2),
              )
            ]
        ),
        child: Padding(
          padding: const EdgeInsets.only(top:20.0),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
            InkWell(onTap:(){
              Navigator.pop(context, MaterialPageRoute( builder: (BuildContext context) =>Settings()));
            },
                child: Icon(Icons.arrow_back_ios,size: 25,color: Theme.of(context).textSelectionColor ,)
            ),
            Text("Your Orders",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize:25,fontWeight: FontWeight.bold)),
                Text("      ")
          ]
          ),

        ),
        ),
              Container(
                height: MediaQuery.of(context).size.height-160,
                child: ListView.builder(
                    itemCount: 3,
                    itemBuilder:(context,i){
                      return Container (
                        width: 315 ,
                        height: 160 ,
                        margin: EdgeInsets.symmetric(horizontal: 20,vertical: 6) ,
                        padding: EdgeInsets.all ( 10 ) ,
                        decoration: BoxDecoration (
                            borderRadius: BorderRadius.all ( Radius.circular ( 10 ) ) ,
                            color: Theme.of ( context ).focusColor
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: Column(

                            children :[
                              SizedBox(height: 2,),
                              Text("Order Number : #122238",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color:Theme.of(context).textSelectionColor )),
                              Text("Order Address : تحت عقب البحر",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color:Theme.of(context).textSelectionColor ),),
                              Text("Order Date : 13/13/2020",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color:Theme.of(context).textSelectionColor )),
                              Text("Order Price : £85",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color:Theme.of(context).textSelectionColor )),
                              SizedBox(height: 5,),
                              Center(
                                child: Container(
                                  height:35,
                                  width: 120,
                                  decoration: BoxDecoration (
                                      borderRadius: BorderRadius.all ( Radius.circular ( 10 ) ),
                                      color: Theme.of(context).accentColor ),
                                  child: Padding(
                                    padding: const EdgeInsets.only(left:15.0),
                                    child: Row(
                                    children: [


                                    Text("Details",style: TextStyle(fontSize: 20, color:  Theme.of(context).primaryColor,fontWeight: FontWeight.bold),),
                                      SizedBox(width: 12,),
                                      Icon(Icons.new_releases,color:  Theme.of(context).primaryColor,)
                                    ],
                                ),
                                  ),),
                              )
                            ]
                          ),
                        ),
                      ) ;
                    }
                ),
              ),
      ]
    ),
    ),
    );
  }
}