import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Account_Screen extends StatefulWidget {
  @override
  _Account_ScreenState createState() => _Account_ScreenState();
}

class _Account_ScreenState extends State<Account_Screen> with SingleTickerProviderStateMixin{
  List<bool> check=[true,true,true,true];
  Widget Field(BuildContext context,String text,String text2,TextInputType type,int i){
    return Padding(
      padding: EdgeInsets.only(bottom: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding:EdgeInsets.only(right: 255),
            child: Text(text2,style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 20),),
          ),
          TextFormField(
            keyboardType: type,
            readOnly:check[i],
            style: TextStyle(color: Theme.of(context).textSelectionColor),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(2),
              hintText: check[i]?text:"",
              hintStyle: TextStyle(fontSize: 20,color:Theme.of(context).highlightColor),
              filled: true,
              suffixIcon: InkWell(onTap: (){ setState(() {
                check[i]=!check[i];
              }); },
                  child: Icon(Icons.edit,color: Theme.of(context).textSelectionColor,size: 30,)
              ),
              fillColor: Theme.of(context).cardColor,
              border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide.none,
              ),
            ),
          ),
        ],
      ),
    );
  }
  bool isSwitched1=false;
  bool isSwitched2=false;
  @override
  Widget build(BuildContext context) {
    return Scaffold (
      backgroundColor: Theme
          .of ( context )
          .primaryColor ,
      body: ListView(
        children: <Widget>[
          Column (
            children: <Widget>[
              Container (
                padding: EdgeInsets.all ( 20 ) ,
                width: MediaQuery
                    .of ( context )
                    .size
                    .width ,
                height: MediaQuery
                    .of ( context )
                    .size
                    .height * 0.12 ,
                decoration: BoxDecoration (
                  color: Theme
                      .of ( context )
                      .primaryColor ,
                  borderRadius: BorderRadius.only (
                      bottomLeft: Radius.circular ( 25 ) ,
                      bottomRight: Radius.circular ( 25 )
                  ) ,
                  boxShadow: [
                    BoxShadow (
                      color: Theme.of(context).backgroundColor,
                      blurRadius: 7 ,
                      spreadRadius: 1 ,
                      offset: Offset ( 0 , 2 ) ,
                    )
                  ] ,
                ) ,
                child: Center (
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      InkWell(onTap: ()=>Navigator.pop(context),child: Icon(Icons.arrow_back,color: Theme.of(context).textSelectionColor,size: 30,)),
                      Text (
                        "Your Account" , style: TextStyle ( fontSize: 24 , color: Theme
                          .of ( context )
                          .textSelectionColor ,fontWeight: FontWeight.w400) , ),
                      InkWell(onTap: ()=>Navigator.pop(context),child: Icon(Icons.check,color: Theme.of(context).accentColor,size: 30,)),
                    ],
                  ) ,
                ) ,
              ) ,
              SizedBox(height: 20,),
              Padding(
                padding: const EdgeInsets.only(right: 150),
                child: Text("Your Information",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 24),),
              ),
              SizedBox(height: 15,),
              Container(
                height: 350,
                width: 350,
                padding: EdgeInsets.only(top: 20,left: 20,right: 0,bottom: 0),
                decoration: BoxDecoration(
                  color: Theme.of(context).cardColor,
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                ),
                child: Column(
                  children: <Widget>[
                    Field(context, "***********","Name   ",TextInputType.text,0),
                    Field(context, "***********","Address",TextInputType.text,1),
                    Field(context, "***********","Email    ",TextInputType.emailAddress,2),
                    Field(context,"***********", "Pass     ",TextInputType.text,3),
                  ],
                ),
              ),
              SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.only(left:5,right: 150,bottom: 10),
                child: Text("Your Preferences",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 24),),
              ),
              Container(
                height: 140,
                width: 350,
                padding: EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                decoration: BoxDecoration(
                  color: Theme.of(context).cardColor,
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Notification",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 20),),
                        Switch(
                          activeColor: Theme.of(context).accentColor,
                          inactiveThumbColor: Theme.of ( context ).textSelectionColor ,
                          inactiveTrackColor:  Theme.of ( context ).textSelectionColor,
                          activeTrackColor: Theme.of(context).accentColor,
                          value: isSwitched1,
                          onChanged: (value) {
                            setState(() {
                              isSwitched1 = value;
                            });
                          },
                        ),
                      ],
                    ) ,
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Newsletter",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 20),),
                        Switch(
                          activeColor: Theme.of(context).accentColor,
                          inactiveThumbColor: Theme.of ( context ).textSelectionColor ,
                          inactiveTrackColor:  Theme.of ( context ).textSelectionColor,
                          activeTrackColor: Theme.of(context).accentColor,
                          value: isSwitched2,
                          onChanged: (value) {
                            setState(() {
                              isSwitched2 = value;
                            });
                          },
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ],
      )
    );
  }
}
