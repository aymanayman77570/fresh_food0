import 'package:flutter/material.dart';
import 'file:///F:/fresh_food-master%20(2)/fresh_food-master/lib/Settings/AppState.dart';
import 'package:provider/provider.dart';
import 'package:fresh2_food/Splash_screen.dart';


void main() {
  runApp(
    ChangeNotifierProvider<AppStateNotifier>(
      child: HomeApp(), create: (BuildContext context) {
          return AppStateNotifier();
    },
    ),
  );
}
class HomeApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<AppStateNotifier>(
      builder: (context, appState, child) {
        return MaterialApp(
          title: "Fresh Food",
          debugShowCheckedModeBanner: false,
          theme: AppTheme.lightTheme,
          darkTheme: AppTheme.darkTheme,
          home: SplashScreen(),
          themeMode: appState.isDarkMode ? ThemeMode.dark: ThemeMode.light,
        );
      },
    );
  }
}

class AppTheme {
  AppTheme();
  static final ThemeData lightTheme = ThemeData(
    primaryColor:Color.fromRGBO(255,255,255,1),
    accentColor: Color.fromRGBO(0,204,51, 1),
    primaryColorLight: Color.fromRGBO(64, 78, 90, 1),
    primaryColorDark:  Color.fromRGBO(78, 93,106, 1),
    focusColor: Color.fromRGBO(240, 244,244, 1),
    backgroundColor: Color.fromRGBO(219, 226,237, 1),
    cursorColor:  Color.fromRGBO(219, 226,237, 1),
    highlightColor: Color.fromRGBO(166, 188, 208, 1),
    textSelectionColor: Color.fromRGBO(116, 138, 157, 1),
    cardColor: Color.fromRGBO(219, 226,237, 1),
  );

  static final ThemeData darkTheme = ThemeData(
    primaryColor:Color.fromRGBO(64, 78, 90, 1),
    accentColor: Color.fromRGBO(123,237,141, 1),
    primaryColorLight: Color.fromRGBO(255,255,255,1),
    primaryColorDark:  Color.fromRGBO(240, 244,244, 1),
    focusColor: Color.fromRGBO(78, 93,106, 1),
    backgroundColor:Color.fromRGBO(78, 93,106, 1),
    cursorColor:Color.fromRGBO(116, 138,157, 1) ,
    highlightColor: Color.fromRGBO(219, 226,237, 1) ,
    textSelectionColor: Color.fromRGBO(166, 188, 208, 1),
    cardColor: Color.fromRGBO(78, 93,106, 1)
  );
}
